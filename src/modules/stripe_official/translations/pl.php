<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{stripe_official}prestashop>stripe_official_cc21116ce900f38c0691823ab193b9a3'] = 'Zapłać kartą';
$_MODULE['<{stripe_official}prestashop>stripe_official_751e90522b3385322c4c34b6f73847d2'] = 'Zapłać za pomocą Przelewy24';
$_MODULE['<{stripe_official}prestashop>stripe_official_fd2bdc826d325902e451236cf6fd774e'] = 'Przetwarzanie...';
$_MODULE['<{stripe_official}prestashop>stripe_official_15f04500d9edaac382bb3d451c8b4198'] = 'Przekierowywanie...';
$_MODULE['<{stripe_official}prestashop>my-account-stripe-cards_a46d3e7285f76d2401202f18b0acbcbd'] = 'Moje karty';
$_MODULE['<{stripe_official}prestashop>payment_form_save_card_6e9c53f6d39367724e9b996b3ea9ef22'] = 'Zapłać kartą';
$_MODULE['<{stripe_official}prestashop>payment_form_save_card_eeceac1af4e7620894d6d2083921bb73'] = 'Zapłać teraz';
$_MODULE['<{stripe_official}prestashop>payment_info_redirect_f3ee3ad3c8443ee27192d35a7a9d86c9'] = 'Zostaniesz przekierowany na stronę operatora płatności w celu dokończenia trasakcji.';
$_MODULE['<{stripe_official}prestashop>order-confirmation_3380b659ba0a8b01af98dbe7494c0706'] = 'Udało Ci się złożyć zamówienie w krótce zacznie się proces pakowania.';
$_MODULE['<{stripe_official}prestashop>order-confirmation_77031dd06e26cfdab21b078a37741bb6'] = 'Numer Twojego zamówienia: [b]@target@[/b].';
$_MODULE['<{stripe_official}prestashop>order-confirmation_2a69d95c0cc64162865573233beb3369'] = ' ';
$_MODULE['<{stripe_official}prestashop>order-confirmation_978dec2eec7a6d7350ceefbbb6b1e1b1'] = ' ';
