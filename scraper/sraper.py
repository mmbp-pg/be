import requests
from bs4 import BeautifulSoup


def get_categories_url(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    categories = []
    cards = soup.find("ul", class_="menu-list large standard")
    category_list = cards.find_all("a")
    for category in category_list:
        # if category.find("span"):
        #     name = category.find("span").text
        #     print(name)
        if category.get("id"):
            if category.get("href"):
                categories.append(url + category["href"])
    return categories


def get_subpages_url(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    subpages = []
    page_select_window = soup.find("ul", class_="paginator")
    if page_select_window is not None:
        pages_indices = page_select_window.find_all("li")
        last_page_index = int(pages_indices[-2].text)
        for i in range(1, last_page_index + 1):
            subpages.append(url + "/" + str(i))
    else:
        subpages.append(url)
    return subpages


def get_products_url(base_url, url):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    products = []
    products_window = soup.find("div", class_="products viewphot s-row")
    product_cards = products_window.find_all("div", recursive=False)
    for product_card in product_cards:
        product_card = product_card.find("a")
        if product_card is not None:
            if product_card.get("href"):
                products.append(base_url + product_card["href"])
    return products


def get_product_info(base_url, url):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    product_info = {}

    name = soup.find("h1", class_="name").text.strip()

    price_window = soup.find("div", class_="price")
    price_per_package = price_window.find("em").text

    price_window = soup.find("div", class_="price-netto")
    netto_per_package = price_window.find("em").text

    # price_window = soup.find("div", class_="unit-price-container gross")
    # price_per_kg = soup.find("span", class_="price").text

    description = ""
    description_window = soup.find("div", itemprop="description")
    description_texts = description_window.find_all("p", recursive=False)
    for text in description_texts:
        text.find_all("span")
        if text is not None:
            for line in text:
                description += line.text.strip()

    image = soup.find("div", class_="mainimg").find("img")["src"]

    brand = soup.find("a", class_="brand")["title"]

    product_info["brand"] = brand
    product_info["name"] = name
    product_info["price_per_package"] = price_per_package.replace("\xa0", " ")
    product_info["netto_per_package"] = netto_per_package.replace("\xa0", " ")
    product_info["description"] = description
    product_info["image"] = base_url + image

    return product_info


if __name__ == '__main__':
    base_url = "https://sklepkawa.pl"
    categories = get_categories_url(base_url)
    for category_url in categories:
        subcategories_url = get_subpages_url(category_url)
        for subcategory_url in subcategories_url:
            products_url = get_products_url(base_url, subcategory_url)
            for product_url in products_url:
                print(product_url)
                product_info = get_product_info(base_url, product_url)
                print(product_info)
